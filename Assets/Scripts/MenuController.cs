using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using TMPro;
using Photon.Realtime;


public class MenuController : MonoBehaviourPunCallbacks
{
    
    [SerializeField] private GameObject UsernameMenu;
    [SerializeField] private GameObject ConnectPanel;
    [SerializeField] private TMP_InputField UsernameInput;
    [SerializeField] private TMP_InputField CreateGameInput;
    [SerializeField] private TMP_InputField JoinGameInput;
    public byte maxPlayers = 4;

    [SerializeField] private GameObject StartButton;


    private void Awake()
    {
        Debug.Log("Connecting to Server");
        PhotonNetwork.ConnectUsingSettings();
    }

    private void Start()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
        ConnectPanel.SetActive(false);
    }

    public override void OnConnectedToMaster()
    {
        Debug.Log("Connected to Server");
    }


    public void ChangeUserNameInput()
    {
        if (UsernameInput.text.Length >= 3)
        {
            StartButton.SetActive(true);
        } else
        {
            StartButton.SetActive(false);
        }
    }

    public void SetUserName()
    {
        UsernameMenu.SetActive(false);
        ConnectPanel.SetActive(true);
        PhotonNetwork.NickName = UsernameInput.text;
        Debug.Log("Player Name is = " + PhotonNetwork.NickName);
    }

    public void CreateRoom()
    {
        PhotonNetwork.CreateRoom(CreateGameInput.text, new RoomOptions { MaxPlayers = maxPlayers }, null);
    }
  
    public void JoinGame()
    {
        RoomOptions roomOptions = new RoomOptions();
        roomOptions.MaxPlayers = maxPlayers;
        PhotonNetwork.JoinOrCreateRoom(JoinGameInput.text, roomOptions, TypedLobby.Default);
    }
   

    public override void OnJoinedRoom()
    {
        PhotonNetwork.LoadLevel("MainGame");
    }

}
