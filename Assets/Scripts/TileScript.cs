using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileScript : MonoBehaviour
{

    private Material currentTile;
    public float speed;
    private float offset;

    // Start is called before the first frame update
    void Start()
    {
        currentTile = GetComponent<Renderer>().material;
    }

    // Update is called once per frame
    void Update()
    {
        offset += 0.001f;
        currentTile.SetTextureOffset ("_MainTex", new Vector2 (offset * speed, 0));
    }
}
