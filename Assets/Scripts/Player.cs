using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using TMPro;


public class Player : MonoBehaviourPunCallbacks
{
    public Rigidbody2D rb;
    public GameObject PlayerCamera;
    public PhotonView photonView;
    public SpriteRenderer sr;
    public TMP_Text PlayerNameText;

    public bool isGrounded = true;
    public float moveSpeed = 5f;
    public float JumpForce = 8f;

    private void Awake()
    {

        if(photonView.IsMine == true)
        {
            PlayerCamera.SetActive(true);
            PlayerNameText.text = PhotonNetwork.NickName;
        } 
        else
        {
            PlayerNameText.text = photonView.Owner.NickName;
            PlayerNameText.color = Color.cyan;
        }
    }

    // Update is called once per frame
    void Update()
    {
   
        if (photonView.IsMine)
        {
            checkInput(); 
        }
    }
    
    private void checkInput()
    {
        var movement = Input.GetAxisRaw("Horizontal");
        transform.position += new Vector3(movement, 0, 0) * Time.deltaTime * moveSpeed;

        // Jump Player
        if(Input.GetKeyDown(KeyCode.Space))
        {
            transform.Translate(Vector3.up * JumpForce * Time.deltaTime, Space.World);      
        }

        if(Input.GetKeyDown(KeyCode.A))
        {
            photonView.RPC("FlipTrue", RpcTarget.AllBuffered);
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
          photonView.RPC("FlipFalse", RpcTarget.AllBuffered);
        }
    }

    [PunRPC]

     void FlipTrue()
    {
        sr.flipX = true;
    }

    [PunRPC]
     void FlipFalse()
    {
        sr.flipX = false;
    }
}
