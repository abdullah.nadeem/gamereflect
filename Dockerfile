FROM nginx:stable-alpine

WORKDIR /app
COPY . /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx.conf /etc/nginx/conf.d
RUN chmod 777 -R /usr/share/nginx/html/Build
#COPY /app/* /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]